import random
import numpy as np
import torch
import torch.nn as nn
import gym
from gym.core import ObservationWrapper
from gym.spaces import Box

import utils
import atari_wrappers
from framebuffer import FrameBuffer


class PreprocessAtariObs(ObservationWrapper):
    def __init__(self, env):
        """A gym wrapper that crops, scales image into the desired shapes and grayscales it."""
        ObservationWrapper.__init__(self, env)

        self.img_size = (1, 80, 80)
        self.observation_space = Box(0.0, 1.0, self.img_size)


    def _to_black_white(self, rgb):
        image = rgb[:, :, 0]
        image[image == 144] = 0
        image[image == 109] = 0
        image[image > 0] = 1
        return image.astype(np.float32)
    

    def _crop(self, img):
        return img[34:194, :]
    

    def _resize(self, img):
        return img[::2, ::2]


    def observation(self, img):
        """what happens to each observation"""

        # Here's what you need to do:
        #  * crop image, remove irrelevant parts
        #  * resize image to self.img_size
        #     (use imresize from any library you want,
        #      e.g. opencv, skimage, PIL, keras)
        #  * cast image to grayscale
        #  * convert image pixels to (0,1) range, float32 type
        
        return self._resize(self._crop(self._to_black_white(img)))[np.newaxis, :]


class DQNAgent(nn.Module):
    def __init__(self, state_shape, n_actions, n_state_frames=4, epsilon=0):

        super().__init__()
        self.epsilon = epsilon
        self.n_actions = n_actions
        self.state_shape = state_shape

        # Define your network body here. Please make sure agent is fully contained here
        # nn.Flatten() can be useful
        #<YOUR CODE>
        self.n_state_frames = n_state_frames
        self.relu = nn.ReLU(inplace=True)
        self.conv1 = nn.Conv2d(in_channels=self.n_state_frames, out_channels=32, kernel_size=8, stride=4)
        self.conv2 = nn.Conv2d(in_channels=32, out_channels=64, kernel_size=4, stride=2)
        self.conv3 = nn.Conv2d(in_channels=64, out_channels=64, kernel_size=3, stride=1)
        self.flatten = nn.Flatten()
        self.dense = nn.Linear(64 * 6 * 6, 512)
        self.out = nn.Linear(512, self.n_actions)
        

    def forward(self, state_t):
        """
        takes agent's observation (tensor), returns qvalues (tensor)
        :param state_t: a batch of 4-frame buffers, shape = [batch_size, 4, h, w]
        """
        # Use your network to compute qvalues for given state
        x = state_t
        
        x = self.conv1(x)
        x = self.relu(x)
        x = self.conv2(x)
        x = self.relu(x)
        x = self.conv3(x)
        x = self.relu(x)
        x = self.flatten(x)
        x = self.dense(x)
        x = self.relu(x)
        x = self.out(x)
        
        #qvalues = <YOUR CODE>
        qvalues = x

        return qvalues
    

    def get_qvalues(self, states):
        """
        like forward, but works on numpy arrays, not tensors
        """
        model_device = next(self.parameters()).device
        states = torch.tensor(states, device=model_device, dtype=torch.float)
        qvalues = self.forward(states)
        return qvalues.data.cpu().numpy()
    

    def sample_actions(self, qvalues):
        """pick actions given qvalues. Uses epsilon-greedy exploration strategy. """
        epsilon = self.epsilon
        batch_size, n_actions = qvalues.shape

        random_actions = np.random.choice(n_actions, size=batch_size)
        best_actions = qvalues.argmax(axis=-1)

        should_explore = np.random.choice(
            [0, 1], batch_size, p=[1-epsilon, epsilon])
        return np.where(should_explore, random_actions, best_actions)



def PrimaryAtariWrap(env, clip_rewards=True):
    assert 'NoFrameskip' in env.spec.id

    # This wrapper holds the same action for <skip> frames and outputs
    # the maximal pixel value of 2 last frames (to handle blinking
    # in some envs)
    env = atari_wrappers.MaxAndSkipEnv(env, skip=4)

    # This wrapper sends done=True when each life is lost
    # (not all the 5 lives that are givern by the game rules).
    # It should make easier for the agent to understand that losing is bad.
    env = atari_wrappers.EpisodicLifeEnv(env)

    # This wrapper laucnhes the ball when an episode starts.
    # Without it the agent has to learn this action, too.
    # Actually it can but learning would take longer.
    env = atari_wrappers.FireResetEnv(env)

    # This wrapper transforms rewards to {-1, 0, 1} according to their sign
    if clip_rewards:
        env = atari_wrappers.ClipRewardEnv(env)

    # This wrapper is yours :)
    env = PreprocessAtariObs(env)
    return env


def make_env(env_name, clip_rewards=True, seed=None):
    env = gym.make(env_name)  # create raw env
    if seed is not None:
        env.seed(seed)
    env = PrimaryAtariWrap(env, clip_rewards)
    env = FrameBuffer(env, n_frames=4, dim_order='pytorch')
    return env


def evaluate(env, agent, n_games=1, greedy=False, t_max=1000000000):
    """ Plays n_games full games. If greedy, picks actions as argmax(qvalues). Returns mean reward. """
    with torch.no_grad():
        rewards = []
        for i in range(n_games):
            s = env.reset()
            reward = 0
            for _ in range(t_max):
                qvalues = agent.get_qvalues([s])
                action = qvalues.argmax(axis=-1)[0] if greedy else agent.sample_actions(qvalues)[0]
                s, r, done, _ = env.step(action)
                reward += r
                if done:
                    break
            
            rewards.append(reward)
        
        return np.mean(rewards)